FROM python:3.11

# Set the working directory to /app
WORKDIR /app

COPY requirements.txt /app
COPY llama_project.py /app
RUN pip install -r requirements.txt

# Run script when the container launches
ENTRYPOINT ["python", "llama_project.py"]
